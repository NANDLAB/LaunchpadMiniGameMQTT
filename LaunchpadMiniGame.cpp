#include "LaunchpadMiniGame.hpp"
#include <boost/bind.hpp>
#include <iomanip>
#include <thread>

void LaunchpadMiniGame::launchpadCallback(std::uint8_t x, std::uint8_t y, std::uint8_t press) {
    std::cout << "("
              << std::setw(2) << unsigned(x) << ","
              << std::setw(2) <<  unsigned(y) << ") : "
              << unsigned(press) << std::endl;
    int key = 10 * y + x + 11;
    if (press) {
        fluid_synth_noteon(fluidSynth, 0, key, press);
    }
    else {
        fluid_synth_noteoff(fluidSynth, 0, key);
    }
}

LaunchpadMiniGame::LaunchpadMiniGame(
        boost::asio::io_context &ioc,
        const std::string &broker_hostname,
        std::uint16_t broker_port,
        const std::string &node_name,
        unsigned midiInPort,
        unsigned midiOutPort,
        const std::string &soundfont
)
    : MQTTNode(ioc, broker_hostname, broker_port, node_name),
      midiInPort(midiInPort), midiOutPort(midiOutPort),
      launchpad(ioc, boost::bind(&LaunchpadMiniGame::launchpadCallback, this, _1, _2, _3)),
      soundfont(soundfont),
      fluidSettings(createFluidSettings()),
      fluidSynth(createFluidSynth()),
      fluidAudioDriver(createFluidAudioDriver())
{}

fluid_settings_t* LaunchpadMiniGame::createFluidSettings() {
    fluid_settings_t *fluidSettings;
    assert(fluidSettings = new_fluid_settings());
    assert(fluid_settings_setstr(fluidSettings, "audio.driver", "alsa") == FLUID_OK);
    return fluidSettings;
}

fluid_synth_t* LaunchpadMiniGame::createFluidSynth() {
    fluid_synth_t *fluidSynth;
    assert(fluidSynth = new_fluid_synth(fluidSettings));
    return fluidSynth;
}

fluid_audio_driver_t* LaunchpadMiniGame::createFluidAudioDriver() {
    fluid_audio_driver_t *fluidAudioDriver;
    assert(fluidAudioDriver = new_fluid_audio_driver(fluidSettings, fluidSynth));
    return fluidAudioDriver;
}

LaunchpadMiniGame::~LaunchpadMiniGame() {
    delete_fluid_audio_driver(fluidAudioDriver);
    delete_fluid_synth(fluidSynth);
    delete_fluid_settings(fluidSettings);
}

void LaunchpadMiniGame::start() {
    MQTTNode::start();
    if(fluid_synth_sfload(fluidSynth, soundfont.data(), 1) == FLUID_FAILED)
    {
        std::cerr << "FLUID: Loading the SoundFont failed!" << std::endl;
        std::exit(1);
    }
    launchpad.open(midiInPort, midiOutPort);
}
